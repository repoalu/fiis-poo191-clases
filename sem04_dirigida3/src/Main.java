import java.util.Arrays;

public class Main {

    // Prob 1
    public static void mostrarNotasBajas(int[] notas, int cantidad){
        Arrays.sort(notas);
        System.out.println(cantidad + " NOTAS MAS BAJAS") ;
        System.out.println("*****************");
        for (int i = 0; i < cantidad; i++){
            System.out.println(notas[i]);
        }
    }



    public static void main(String[] args) {
        int[] notas = {20, 10, 13, 14, 15 ,12, 18, 19, 20};
        mostrarNotasBajas(notas, 3);
    }
}
