package edu.uni.poo.dirigida1;

public class PruebaTrianguloV2 {

    public static void main(String[] args) {
        TrianguloV2 triangulo = new TrianguloV2(3,4,5);
        System.out.println(triangulo.obtenerArea());
        System.out.println(triangulo.obtenerPerimetro());
        System.out.println(triangulo.obtenerTipoTriangulo());
    }
}
