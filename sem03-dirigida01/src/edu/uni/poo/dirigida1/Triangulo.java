package edu.uni.poo.dirigida1;

public class Triangulo {
    double lado1;
    double lado2;
    double lado3;

    public Triangulo(double l1, double l2, double l3){
        lado1 = l1;
        lado2 = l2;
        lado3 = l3;
    }

    public double obtenerPerimetro(){
        return lado1 + lado2 + lado3;
    }


    public static double perimetro(double lado1, double lado2, double lado3){
        return lado1 + lado2 + lado3;
    }


    public static void main(String[] args) {
        System.out.println("Hello World!" + Math.abs(-3));
        System.out.println("El perimetro es : " + perimetro(2,5,6));
    }
}
