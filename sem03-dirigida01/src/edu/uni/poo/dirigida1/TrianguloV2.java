package edu.uni.poo.dirigida1;

public class TrianguloV2 {

    double lado1;
    double lado2;
    double lado3;

    public TrianguloV2(double a, double b, double c){
        lado1 = a;
        lado2 = b;
        lado3 = c;
    }

    public double obtenerArea(){
        double p = obtenerPerimetro() / 2;
        double area = Math.sqrt(p * (p - lado1) * (p - lado2) * (p - lado3));
        return area;
    }

    public double obtenerPerimetro(){
        return lado1 + lado2 + lado3;
    }

    public String obtenerTipoTriangulo(){
        if(lado1 == lado2 && lado2 == lado3){
            return "equilatero";
        }else if(lado1 == lado2 || lado2 == lado3 || lado1 == lado3){
            return "isosceles";
        }else{
            return "escaleno";
        }
    }
}
