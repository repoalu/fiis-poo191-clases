package edu.uni.poo.dirigida1.edu.uni.poo.dirigida1.test;
import edu.uni.poo.dirigida1.TrianguloV1;

public class PruebaTrianguloV1 {
    public static void main(String[] args) {
        double lado1 = 3;
        double lado2 = 4;
        double lado3 = 5;
        System.out.println(TrianguloV1.obtenerArea(lado1, lado2, lado3));
        System.out.println(TrianguloV1.obtenerPerimetro(lado1, lado2, lado3));
        System.out.println(TrianguloV1.obtenerTipoTriangulo(lado1, lado2, lado3));
    }
}
