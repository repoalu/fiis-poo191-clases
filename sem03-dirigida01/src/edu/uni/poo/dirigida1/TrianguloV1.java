package edu.uni.poo.dirigida1;

public class TrianguloV1 {

    public static double obtenerArea(double lado1, double lado2, double lado3){
        double p = obtenerPerimetro(lado1, lado2, lado3) / 2;
        double area = Math.sqrt(p * (p - lado1) * (p - lado2) * (p - lado3));
        return area;
    }

    public static double obtenerPerimetro(double lado1, double lado2, double lado3){
        return lado1 + lado2 + lado3;
    }

    public static String obtenerTipoTriangulo(double lado1, double lado2, double lado3){
        if(lado1 == lado2 && lado2 == lado3){
            return "equilatero";
        }else if(lado1 == lado2 || lado2 == lado3 || lado1 == lado3){
            return "isosceles";
        }else{
            return "escaleno";
        }

    }
}
